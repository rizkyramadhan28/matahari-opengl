from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import math


def init():
    glClearColor(0.0, 0.0, 0.0, 0.0)
    gluOrtho2D(-500.0, 500.0, -500.0, 500.0)


def plotpoints():

    # Warna kuning

    glColor3f(1., 1., 0.)

    # Sudut yang akan digunakan untuk rotasi segitiga
    # sehingga membentuk matahari

    list_of_sudut = [0, 20, 40, 60, 80, 100, 120, 140,
                     160, 180, 200, 220, 240, 260, 280, 300, 320, 260, 240, 220, 200, 180, 100]

    for sudut in list_of_sudut:
        glRotatef(sudut, 0., 0., 1.)

        # Gunakan syntax di bawah untuk membuat matahari
        # versi line.

        # glBegin(GL_LINE_LOOP)

        # Membuat segitiga dengan warna.

        glBegin(GL_TRIANGLES)

        glVertex2f(-50., 0.)
        glVertex2f(50., 0.)
        glVertex2f(0., 200.)

        glEnd()

    glFlush()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(500, 500)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("Matahari")
    glutDisplayFunc(plotpoints)

    init()
    glutMainLoop()


main()
